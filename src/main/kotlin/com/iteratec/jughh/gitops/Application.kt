package com.iteratec.jughh.gitops

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("com.iteratec.jughh.gitops")
                .mainClass(Application.javaClass)
                .start()
    }
}